/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registro;

/**
 *
 * @author Hector Ramirez
 */
public class Registro {

    private int codigo;
    private int cantidad;
    private int tipo;
    private float precio;

    public Registro(){
        this.codigo = 0;
        this.cantidad = 0;
        this.tipo = 0;
        this.precio = 0.0f;
    }
    
    public Registro(int codigo, int cantidad, int tipo, float precio) {
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    public Registro(Registro x) {
        this.codigo = x.codigo;
        this.cantidad = x.cantidad;
        this.tipo = x.tipo;
        this.precio = x.precio;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public float calcularCosto(){
        float costo=0.0f;
        costo= this.cantidad * this.precio;
        return costo;
    }
    
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto= this.calcularCosto() * .16f;
        return impuesto;
    }
    
    public float calcularTotal(){
        float total=0.0f;
        total = this.calcularCosto() + this.calcularImpuesto();
        return total;
    }
}
